///-----------------------------------------------------------------
/// Author : Clément Martin
/// Date : 23/06/2022 16:29
///-----------------------------------------------------------------

using System;
using UnityEngine;

namespace Com.IsartDigital.BalleQuiRebondit {
	public class Player : MonoBehaviour
	{
		[SerializeField] private float explosionRadius;
		[SerializeField] private Transform sphereRadius;
		[SerializeField] private float speedMax = 1f;
		[SerializeField] private float friction = 0.98f;
		[SerializeField] private string tagPlayer = "Player";
		[SerializeField] private string tagWall = "Wall";
		[SerializeField] private float minCoeffSpeed = 0.3f;
		
		private Vector3 acceleration = new Vector3();
		public Vector3 velocity = new Vector3();

		private Action DoAction;
		private Vector3 toTarget;

		public static Player Instance { get; private set; }

        private void Awake()
        {
			Instance = this;
		}

		private void Start()
		{
			sphereRadius.localScale = new Vector3(explosionRadius * 2, 1, explosionRadius * 2);

			SetModeVoid();
		}

		private void Update()
		{
            Debug.DrawRay(Camera.main.ScreenPointToRay(Input.mousePosition).origin, Camera.main.ScreenPointToRay(Input.mousePosition).direction * 1000f, Color.red);

			RaycastHit hit;

			if (Input.GetMouseButtonDown(0) && Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, 1000f))
			{
				if (!hit.collider.CompareTag(tagPlayer))
				{
					Camera camera = Camera.main;

					Vector3 screenPosition = Input.mousePosition;
					screenPosition.z = camera.transform.position.y - transform.position.y;

					Vector3 origin = camera.ScreenToWorldPoint(screenPosition);

					sphereRadius.position = origin;

					origin.y = 0;
					Vector3 positionWithoutY = new Vector3(transform.position.x, 0, transform.position.z);

					float distanceToOrigin = Vector3.Distance(positionWithoutY, origin);

					if (distanceToOrigin <= explosionRadius)
					{
						Vector3 toTarget = positionWithoutY - origin;

						Debug.DrawRay(origin, toTarget, Color.blue, 10f);

						SetModeMove(toTarget, distanceToOrigin,speedMax);
					}
				}

			}

			DoAction();
		}

		private void SetModeVoid()
        {
			DoAction = DoActionVoid;
        }

		private void DoActionVoid()
        {

        }

		public void SetModeMove(Vector3 toTarget, float distanceToOrigin, float speed)
        {
			this.toTarget = toTarget.normalized;

			float coefficient = Mathf.Clamp( 1 - distanceToOrigin / explosionRadius, minCoeffSpeed,1f);

			acceleration = toTarget.normalized * (speed * coefficient);
			velocity = acceleration;

			Quaternion orientation = Quaternion.LookRotation(this.toTarget,Vector3.up);

			transform.rotation = orientation;

			DoAction = DoActionMove;
        }

		private void DoActionMove()
        {
            velocity *= friction;

			if (velocity == Vector3.zero)
			{
				velocity = Vector3.zero;
				SetModeVoid();
			}

			transform.position += velocity * Time.deltaTime;
		}

        private void OnCollisionEnter(Collision collision)
        {
			if (collision.collider.CompareTag(tagWall))
			{
				velocity = Vector3.Reflect(velocity, collision.transform.forward);

				Quaternion orientation = Quaternion.LookRotation(velocity, Vector3.up);

				transform.rotation = orientation;
			}
		}
    }
}



