///-----------------------------------------------------------------
/// Author : Clément Martin
/// Date : 24/06/2022 18:11
///-----------------------------------------------------------------

using UnityEngine;

namespace Com.IsartDigital.BalleQuiRebondit.Enemy {
	public abstract class Enemy : MonoBehaviour {
		[SerializeField] protected float speed;

		protected abstract void Move();

	}
}



