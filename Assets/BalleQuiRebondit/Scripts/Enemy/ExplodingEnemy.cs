///-----------------------------------------------------------------
/// Author : Clément Martin
/// Date : 30/06/2022 18:33
///-----------------------------------------------------------------

using System.Collections.Generic;
using UnityEngine;

namespace Com.IsartDigital.BalleQuiRebondit.Enemy {
	public delegate void ExplodingEnemyEventHandler(ExplodingEnemy sender, float speed, Vector3 toTarget,float distanceToOrigin);

	public class ExplodingEnemy : Enemy {
		[SerializeField] private string tagWall = "Wall";
		[SerializeField] private float radiusExplosion = 100;
		[SerializeField] private float speedExplosion;

        private Transform target;

		public static List<ExplodingEnemy> allExplodingEnemy = new List<ExplodingEnemy>();

		public event ExplodingEnemyEventHandler explosionEnemy;

		private void Awake()
        {
			allExplodingEnemy.Add(this);
		}

		private void Start () {
			transform.rotation = Quaternion.AngleAxis(Random.Range(0, 360f), Vector3.up);
			target = Player.Instance.transform;
		}

		private void Update () {
			Move();
		}

		protected override void Move()
		{
			transform.position += transform.forward * speed * Time.deltaTime;
		}

        private void OnCollisionEnter(Collision collision)
        {
            if (collision.collider.CompareTag(tagWall))
            {
				Explode();
            }
        }

		private void Explode()
        {
			Vector3 positionWithoutY = transform.position;
			positionWithoutY.y = 0;

			Vector3 targetPosition = target.position;
			targetPosition.y = 0;

			float distanceToOrigin = Vector3.Distance(positionWithoutY, targetPosition);

			if (Vector3.Distance(positionWithoutY, targetPosition) <= radiusExplosion)
            {
				explosionEnemy?.Invoke(this, speedExplosion,targetPosition - positionWithoutY, distanceToOrigin);

				//Player.Instance.velocity = ().normalized * speedExplosion;
			}

			Destroy(gameObject);
        }
    }
}



