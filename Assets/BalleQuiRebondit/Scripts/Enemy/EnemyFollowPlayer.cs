///-----------------------------------------------------------------
/// Author : Clément Martin
/// Date : 24/06/2022 18:12
///-----------------------------------------------------------------

using UnityEngine;

namespace Com.IsartDigital.BalleQuiRebondit.Enemy {
	public class EnemyFollowPlayer : Enemy {
		private Transform target;

        private void Start()
        {
			target = Player.Instance.transform;
        }

        private void Update () {
            Move();
		}

        protected override void Move()
        {
            transform.position = Vector3.MoveTowards(transform.position, target.position, speed * Time.deltaTime);

            Vector3 toTarget = target.transform.position - transform.position;

            if (toTarget != Vector3.zero)
                transform.rotation = Quaternion.LookRotation(toTarget);
        }
    }
}



