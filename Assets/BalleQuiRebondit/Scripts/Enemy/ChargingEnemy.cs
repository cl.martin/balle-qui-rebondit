///-----------------------------------------------------------------
/// Author : Clément Martin
/// Date : 29/06/2022 18:33
///-----------------------------------------------------------------

using UnityEngine;

namespace Com.IsartDigital.BalleQuiRebondit.Enemy {
	public class ChargingEnemy : Enemy {
		[SerializeField] private float speedCharge = 15;
		[SerializeField] private float timeToCharge;
		[SerializeField] private float timeWhileYoureStuck;
		[SerializeField] private float timeWhileYourCharge;
		[SerializeField] private float timeChangeDirection;
		[SerializeField] private string tagWall= "Wall";

		private Vector3 target;
		private float elapsedTimeToCharge;
		private float elapsedTimeStuck;
		private float elapsedTimeWhileYourCharge;
		private float elapsedTimeChangeDirection;

		private void Start () {
			target = Player.Instance.transform.position;

            transform.rotation = Quaternion.AngleAxis(Random.Range(0, 360f), Vector3.up);
        }
	
		private void Update () {
			Move();
		}

		protected override void Move()
		{
			elapsedTimeToCharge += Time.deltaTime;

			if (elapsedTimeToCharge <= timeToCharge)
			{
				if (elapsedTimeChangeDirection >= timeChangeDirection)
                {
					elapsedTimeChangeDirection = 0;

					transform.rotation = Quaternion.AngleAxis(Random.Range(0, 360f), Vector3.up);
				}
				else
                {
					elapsedTimeChangeDirection += Time.deltaTime;

					transform.position += transform.forward * speed * Time.deltaTime;
				}
				

				//transform.position = Vector3.MoveTowards(transform.position, target, speed * Time.deltaTime);

				//Vector3 toTarget = target - transform.position;

				//if (toTarget != Vector3.zero)
				//	transform.rotation = Quaternion.LookRotation(toTarget);
            }
            else
            {
				elapsedTimeStuck += Time.deltaTime;

				if (elapsedTimeStuck >= timeWhileYoureStuck)
                {
					elapsedTimeWhileYourCharge += Time.deltaTime;

					transform.position += transform.forward * speedCharge * Time.deltaTime;

					if (elapsedTimeWhileYourCharge >= timeWhileYourCharge)
                    {
						elapsedTimeStuck = 0;
						elapsedTimeToCharge = 0;
						elapsedTimeWhileYourCharge = 0;
					}

				}
                else
                {
					target = Player.Instance.transform.position;

					Vector3 toTarget = target - transform.position;

					transform.rotation = Quaternion.LookRotation(toTarget);
				}
			}
		}

        private void OnCollisionEnter(Collision collision)
        {
            if (collision.collider.CompareTag(tagWall))
            {
				transform.rotation = transform.rotation * Quaternion.AngleAxis(180f, Vector3.up);
            }
        }
    }
}



