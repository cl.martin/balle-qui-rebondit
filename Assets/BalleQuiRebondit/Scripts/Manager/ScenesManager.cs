///-----------------------------------------------------------------
/// Author : Adrien Lemaire
/// Date : 09/06/2022 22:35
///-----------------------------------------------------------------

using Com.IsartDigital.BalleQuiRebondit.UI.UIScreen;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Com.IsartDigital.BalleQuiRebondit.Manager {
	public class ScenesManager : MonoBehaviour 
	{
		private const string UISceneName = "UI";
		private const string GamePlaySceneName = "GamePlay";

        #region Unity methods
        public void Start() {
            LoadingScreen.OnBeforeLoading += LoadingScreen_OnBeforeLoading;
            LoadingScreen.OnAfterLoading += LoadingScreen_OnAfterLoading;
            SceneManager.sceneLoaded += SceneManager_sceneLoaded;

			UIManager.Instance.Init();

			LoadingScreen.Instance.gameObject.SetActive(false);
		}
        #endregion

        #region Event
        private void LoadingScreen_OnBeforeLoading(LoadingScreen sender)
		{
			switch (SceneManager.GetActiveScene().name)
			{
				case UISceneName:
					SceneManager.LoadScene(GamePlaySceneName);
					break;
				case GamePlaySceneName:
					SceneManager.LoadScene(UISceneName);
					break;
			}
		}

		private void SceneManager_sceneLoaded(Scene arg0, LoadSceneMode arg1)
		{
			switch (arg0.name)
			{
				case UISceneName:
					UIManager uiManager = UIManager.Instance;

					uiManager.mainMenu.GetComponentInParent<Canvas>().worldCamera = Camera.main;
					uiManager.SetFirstScreen(uiManager.mainMenu);
					break;
				case GamePlaySceneName:
					GamePlayManager.Instance.hud.GetComponentInParent<Canvas>().worldCamera = Camera.main;
					break;
			}

			LoadingScreen.Instance.StartDeactivate();
		}

		private void LoadingScreen_OnAfterLoading(LoadingScreen sender)
		{
			switch (SceneManager.GetActiveScene().name)
			{
				case UISceneName:
					UIManager.Instance.mainMenu.ActivateButtons();
					break;
				case GamePlaySceneName:
					GamePlayManager gamePlayManager = GamePlayManager.Instance;

					gamePlayManager.Init();
					gamePlayManager.startInit = false;
					break;
			}

			LoadingScreen.Instance.gameObject.SetActive(false);
		}
		#endregion
	}
}