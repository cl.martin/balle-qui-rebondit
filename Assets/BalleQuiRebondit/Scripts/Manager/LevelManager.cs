///-----------------------------------------------------------------
/// Author : Adrien Lemaire
/// Date : 23/06/2022 17:20
///-----------------------------------------------------------------

using System.Collections;
using UnityEngine;

namespace Com.IsartDigital.BalleQuiRebondit.Manager {

	public delegate void LevelManagerEventHandler(LevelManager sender);

	public class LevelManager : MonoBehaviour 
	{

		public static event LevelManagerEventHandler OnFinishCreatingLevel;

		#region Unity Methods
		public void Init () {
			
		}
		
		public void GameLoop () {
			
		}
		#endregion
		
		#region Events
		#endregion
	}
}

