///-----------------------------------------------------------------
/// Author : Adrien Lemaire
/// Date : 10/06/2022 23:57
///-----------------------------------------------------------------

using Com.IsartDigital.BalleQuiRebondit.Enemy;
using Com.IsartDigital.BalleQuiRebondit.UI.UIScreen;
using System.Collections.Generic;
using UnityEngine;

namespace Com.IsartDigital.BalleQuiRebondit.Manager {
	public class GamePlayManager : MonoBehaviour 
	{
		public static GamePlayManager Instance = default;

        [SerializeField] private LevelManager levelManager = default;

        public Hud hud = default;

        public bool startInit = true;

        #region Unity methods
        private void Awake()
        {
			Instance = this;
        }

        private void Start()
        {
            LevelManager.OnFinishCreatingLevel += LevelManager_OnFinishCreatingLevel;

            List<ExplodingEnemy> allExplodingEnemy = ExplodingEnemy.allExplodingEnemy;

            for (int i = 0; i < allExplodingEnemy.Count; i++)
            {
                allExplodingEnemy[i].explosionEnemy += GamePlayManager_explosionEnemy;
            }

            if (startInit) Init();
        }

        private void GamePlayManager_explosionEnemy(ExplodingEnemy sender, float speed, Vector3 toTarget, float distanceToOrigin)
        {
            Player.Instance.SetModeMove(toTarget, distanceToOrigin, speed);
        }

        public void Init()
        {
            levelManager.Init();

            hud.ActivateButtons();
        }
        #endregion

        #region Events
        private void LevelManager_OnFinishCreatingLevel(LevelManager sender)
        {
            PlayGame();   
        }
        #endregion

        private void PlayGame()
        {

        }
    }
}

