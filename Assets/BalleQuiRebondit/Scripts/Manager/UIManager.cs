///-----------------------------------------------------------------
/// Author : Adrien Lemaire
/// Date : 09/06/2022 23:48
///-----------------------------------------------------------------

using Com.IsartDigital.BalleQuiRebondit.UI.UIScreen;
using UnityEngine;

namespace Com.IsartDigital.BalleQuiRebondit.Manager {
	public class UIManager : MonoBehaviour 
	{
		[SerializeField] private BaseScreen firstScreenActive = default;

		public static UIManager Instance = default;

		public TitleCard titleCard = default;
		public MainMenu mainMenu = default;
		public Options options = default;

        #region Unity methods
        private void Awake()
        {
			Instance = this;
        }

        public void Init () {
			SetFirstScreen();
		}
		#endregion

		public void SetFirstScreen(BaseScreen screen = null)
        {
			if (!screen) screen = firstScreenActive;

			Animator animator = screen.GetComponent<Animator>();

			animator.SetTrigger(BaseScreen.startIn);
			animator.SetTrigger(BaseScreen.active);

			screen.ActivateButtons();
			screen.EndActivate();
		}
	}
}

