///-----------------------------------------------------------------
/// Author : Adrien Lemaire
/// Date : 09/06/2022 22:36
///-----------------------------------------------------------------

using UnityEngine;
using UnityEngine.UI;

namespace Com.IsartDigital.BalleQuiRebondit.UI.UIScreen {
	public class TitleCard : BaseScreen
    {
        [SerializeField] private MainMenu mainMenu = default;

        [SerializeField] private Button playButton = default;
        [SerializeField] private Button exitButton = default;

        #region Active screen
        public override void ActivateButtons()
        {
            playButton.onClick.AddListener(Play);
            exitButton.onClick.AddListener(Exit);
        }

        public override void DeactivateButtons()
        {
            playButton.onClick.RemoveListener(Play);
            exitButton.onClick.RemoveListener(Exit);
        }
        #endregion

        #region Buttons
        private void Play()
        {
            StartDeactivate();
            mainMenu.StartActivate();
        }

        private void Exit()
        {
            Debug.Log("quit");
            Application.Quit();
        }
        #endregion
    }
}

