///-----------------------------------------------------------------
/// Author : Adrien Lemaire
/// Date : 09/06/2022 22:36
///-----------------------------------------------------------------

using UnityEngine;
using UnityEngine.UI;

namespace Com.IsartDigital.BalleQuiRebondit.UI.UIScreen {
	public class Options : BaseScreen
    {
        [SerializeField] private MainMenu mainMenu = default;

        [SerializeField] private Button exitButton = default;

        #region Active screen
        public override void ActivateButtons()
        {
            exitButton.onClick.AddListener(Exit);
        }

        public override void DeactivateButtons()
        {
            exitButton.onClick.RemoveListener(Exit);
        }
        #endregion

        #region Buttons
        private void Exit()
        {
            StartDeactivate();
            mainMenu.StartActivate();
        }
        #endregion
    }
}

