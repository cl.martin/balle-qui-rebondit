///-----------------------------------------------------------------
/// Author : Adrien Lemaire
/// Date : 09/06/2022 22:36
///-----------------------------------------------------------------

using UnityEngine;
using UnityEngine.UI;

namespace Com.IsartDigital.BalleQuiRebondit.UI.UIScreen {
	public class Hud : BaseScreen
    {
        [SerializeField] private Button exitButton = default;

        public static event BaseScreenEventHandler OnExit = default;

        #region Active screen
        public override void ActivateButtons()
        {
            exitButton.onClick.AddListener(ReturnToMenu);
        }

        public override void DeactivateButtons()
        {
            exitButton.onClick.RemoveListener(ReturnToMenu);
        }
        #endregion

        #region Buttons
        private void ReturnToMenu()
        {
            DeactivateButtons();

            LoadingScreen.Instance.gameObject.SetActive(true);
            LoadingScreen.Instance.StartActivate();
        }
        #endregion

        private void Update()
        {
            if (Input.GetMouseButtonDown(1))
            {
                Debug.Log("Click droit");
            }
        }
    }
}

