///-----------------------------------------------------------------
/// Author : Adrien Lemaire
/// Date : 09/06/2022 22:36
///-----------------------------------------------------------------

using UnityEngine;
using UnityEngine.UI;

namespace Com.IsartDigital.BalleQuiRebondit.UI.UIScreen {
	public class MainMenu : BaseScreen
	{
        [SerializeField] private TitleCard titleCard = default;
        [SerializeField] private Options options = default;

		[SerializeField] private Button startButton = default;
		[SerializeField] private Button optionsButton = default;
		[SerializeField] private Button exitButton = default;

        #region Active screen
        public override void ActivateButtons()
        {
            startButton.onClick.AddListener(StartGame);
            optionsButton.onClick.AddListener(Options);
            exitButton.onClick.AddListener(Exit);
        }

        public override void DeactivateButtons()
        {
            startButton.onClick.RemoveListener(StartGame);
            optionsButton.onClick.RemoveListener(Options);
            exitButton.onClick.RemoveListener(Exit);
        }
        #endregion

        #region Buttons
        private void StartGame()
        {
            DeactivateButtons();

            LoadingScreen.Instance.gameObject.SetActive(true);
            LoadingScreen.Instance.StartActivate();
        }

        private void Options()
        {
            StartDeactivate();
            options.StartActivate();
        }

        private void Exit()
        {
            StartDeactivate();
            titleCard.StartActivate();
        }
        #endregion
    }
}

