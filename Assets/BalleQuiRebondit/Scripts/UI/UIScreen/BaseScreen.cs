///-----------------------------------------------------------------
/// Author : Adrien Lemaire
/// Date : 09/06/2022 23:29
///-----------------------------------------------------------------

using System;
using System.Collections;
using UnityEngine;

namespace Com.IsartDigital.BalleQuiRebondit.UI.UIScreen {
	public delegate void BaseScreenEventHandler(BaseScreen sender, BaseScreenEventArgs args);

	public abstract class BaseScreen : MonoBehaviour 
	{
		public static readonly string active = "IsActive";
		public static readonly string startIn = "StartIn";

		#region active
		public virtual void StartActivate()
		{
			GetComponent<Animator>().SetTrigger(active);

			StartCoroutine(WaitAnimEnd(EndActivate));

			ActivateButtons();
		}

		public virtual void StartDeactivate()
		{
			GetComponent<Animator>().ResetTrigger(active);

			StartCoroutine(WaitAnimEnd(EndDeactivate));

			DeactivateButtons();
		}

		public virtual void ActivateButtons() { }


		//These methods are called when the animation has finished
		public virtual void EndActivate() { }

		public virtual void EndDeactivate() { }

		public virtual void DeactivateButtons() { }
        #endregion

        #region Coroutine
        private IEnumerator WaitAnimEnd(Action action)
        {
			yield return new WaitForSeconds(1);

			action();
        }
		#endregion
	}

	public class BaseScreenEventArgs : EventArgs
    {
		//parameters that you want to pass
    }
}

