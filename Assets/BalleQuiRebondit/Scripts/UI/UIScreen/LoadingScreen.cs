///-----------------------------------------------------------------
/// Author : Adrien Lemaire
/// Date : 09/06/2022 22:37
///-----------------------------------------------------------------

using System.Collections;
using UnityEngine;
using UnityEngine.Events;

namespace Com.IsartDigital.BalleQuiRebondit.UI.UIScreen {
    public delegate void LoadingScreenEventHandler(LoadingScreen sender);

	public class LoadingScreen : MonoBehaviour 
	{
        private static readonly string active = "IsActive";

        private static readonly float timeForAnimation = 1f;

        public static event LoadingScreenEventHandler OnBeforeLoading = default;
        public static event LoadingScreenEventHandler OnAfterLoading = default;

        public static LoadingScreen Instance = default;

        #region Unity methods
        private void Awake()
        {
            DontDestroyOnLoad(transform.root.gameObject);

            if (Instance == null) Instance = this;
            else Destroy(transform.root.gameObject);
        }
        #endregion

        #region Active screen
        public void StartActivate()
        {
            GetComponent<Animator>().SetTrigger(active);
        }

        public void StartDeactivate()
        {
            GetComponent<Animator>().ResetTrigger(active);
        }
        #endregion

        #region Coroutine
        public IEnumerator WaitBeforeLoading()
        {
            yield return new WaitForSeconds(timeForAnimation);

            OnBeforeLoading?.Invoke(this);
        }

        public IEnumerator WaitAfterLoading()
        {
            yield return new WaitForSecondsRealtime(timeForAnimation);

            OnAfterLoading?.Invoke(this);
        }
        #endregion
    }
}

